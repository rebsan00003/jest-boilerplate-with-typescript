
import { secondLargest } from '.'

test('Should take in an array of [10, 40, 30, 20, 50] and return 40', () => {
    //Arrange
    const input = [10, 40, 30, 20, 50];
    const expected = 40;

    //Act
    const actual = secondLargest(input);

    //Assert
    expect(actual).toBe(expected);
});

test('Should take in an array of [25 , 143, 89 ,13 , 105] and return 105', () => {
    //Arrange
    const input = [25 , 143, 89 ,13 , 105];
    const expected = 105;

    //Act
    const actual = secondLargest(input);

    //Assert
    expect(actual).toBe(expected);
});

test('Should take in an array of [54, 23, 11, 17, 10] and return 23', () => {
    //Arrange
    const input = [54, 23, 11, 17, 10];
    const expected = 23;

    //Act
    const actual = secondLargest(input);

    //Assert
    expect(actual).toBe(expected);
});

test('Should take in an array with only two elements that are equal and return 0', () => {
    //Arrange
    const input = [1 ,1];
    const expected = 0;

    //Act
    const actual = secondLargest(input);

    //Assert
    expect(actual).toBe(expected);
});

test('Should take in an array with only one element and return that number', () => {
    //Arrange
    const input = [1];
    const expected = 1;

    //Act
    const actual = secondLargest(input);

    //Assert
    expect(actual).toBe(expected);
});

test('Should take in an empty array and return 1', () => {
    //Arrange
    const input = [];
    const expected = 0;

    //Act
    const actual = secondLargest(input);

    //Assert
    expect(actual).toBe(expected);
});
