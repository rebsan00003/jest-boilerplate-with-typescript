
export const secondLargest = (numberArr) => {

    if(numberArr.length === 0){
        return 0;
    }else if(numberArr.length === 1){
        return numberArr[0]
    }else if(numberArr.length === 2 && numberArr[0] === numberArr[1]){
        return 0;
    }else {
        numberArr.sort(function(a, b){return a - b});
        return numberArr[numberArr.length - 2];
    }
}
