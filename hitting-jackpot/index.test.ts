/*
testJackpot(["@", "@", "@", "@"]) ➞ true
testJackpot(["abc", "abc", "abc", "abc"]) ➞ true
testJackpot(["SS", "SS", "SS", "SS"]) ➞ true
testJackpot(["&&", "&", "&&&", "&&&&"]) ➞ false
testJackpot(["SS", "SS", "SS", "Ss"]) ➞ false

*/
import { testJackpot } from '.'

it("Four @s should return true.", () => {
    const equal = testJackpot(["@","@","@","@"]);
    expect(equal).toBe(true);
})










