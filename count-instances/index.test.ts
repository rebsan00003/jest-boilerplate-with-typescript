
import {charCount} from '.'

test('Should count occurrence of character "a" in string "edabit" and return 1', () => {
    //Arrange
    const inputFirst = "a";
    const inputSecond = "edabit";
    const expected = 1;

    //Act
    const actual = charCount(inputFirst, inputSecond);

    //Assert
    expect(actual).toBe(expected);
});

test('Should count occurrence of character "c" in string "Chamber of secrets" and return 1', () => {
    //Arrange
    const inputFirst = "c";
    const inputSecond = "Chamber of secrets";
    const expected = 1;

    //Act
    const actual = charCount(inputFirst, inputSecond);

    //Assert
    expect(actual).toBe(expected);
});

test('Should count occurrence of character "B" in string "big fat bubble" and return 0', () => {
    //Arrange
    const inputFirst = "B";
    const inputSecond = "big fat bubble";
    const expected = 0;

    //Act
    const actual = charCount(inputFirst, inputSecond);

    //Assert
    expect(actual).toBe(expected);
});

test('Should count occurrence of character "e" in string "javascript is good" and return 0', () => {
    //Arrange
    const inputFirst = "e";
    const inputSecond = "javascript is good";
    const expected = 0;

    //Act
    const actual = charCount(inputFirst, inputSecond);

    //Assert
    expect(actual).toBe(expected);
});

test('Should count occurrence of character "!" in string "!easy!" and return 2', () => {
    //Arrange
    const inputFirst = "!";
    const inputSecond = "!easy!";
    const expected = 2;

    //Act
    const actual = charCount(inputFirst, inputSecond);

    //Assert
    expect(actual).toBe(expected);
});

test('Should throw an error when first string is "wow"', () => {
    //Arrange
    const inputFirst = "wow";
    const inputSecond = "the universe is wow";
    const expected = "error";

    //Act
    const actual = charCount(inputFirst, inputSecond);

    //Assert
    expect(actual).toBe(expected);
});