/*
const string1 = "edabit";
const string2 = "Chamber of secrets";
const string3 = "boxes are fun";
const string4 = "big fat bubble";
const string5 = "!easy!";
*/

export const charCount = (firstString, secondString) => {
    let count = 0;
    if(firstString.length > 1){
        return "error";
    }
    else{  
        for(let i = 0; i < secondString.length; i++){
            if(secondString[i] === firstString){
                count++;
            }
        }
        return count;
    }
}

//console.log(charCount("!", string5));

