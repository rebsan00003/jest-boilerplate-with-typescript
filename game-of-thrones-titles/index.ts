

export const correctTitle = (sentence) => {
    //Sets all characters to lower case, then replaces all commas without whitespace with commas with whitespace, then split the sentence into array
    let lowerCased = sentence.toLowerCase();

    //add whitespace to comma to be able to split string correctly
    let spacedSentence = lowerCased.replaceAll(',', ', ');
    
    //split the string into array
    let sentenceArr = spacedSentence.split(" ");

    //loop through the array of words, if the word is not "and", "of", "in", "the" then make first character of word uppercase
    for (let i = 0; i < sentenceArr.length; i++) {
        
        //if the array contains an empty element then remove it
        if(sentenceArr[i] === ""){
            sentenceArr.splice(i, 1);
        }
        if(sentenceArr[i] != "and" && sentenceArr[i] != "the" && sentenceArr[i] != "of" && sentenceArr[i] != "in"){
            sentenceArr[i] = sentenceArr[i].charAt(0).toUpperCase() + sentenceArr[i].slice(1);
        }
        
        //trim whitespace
        sentenceArr[i].trim();
    }

    //join sentence
    let newSentence = sentenceArr.join(" ");

    //if the sentence does not end with "." then add to end of string
    if(!newSentence.endsWith(".")){
        newSentence += ".";
    }

    return newSentence;
}


