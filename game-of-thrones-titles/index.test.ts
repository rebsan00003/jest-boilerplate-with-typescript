import {correctTitle} from '.'


test('Should correct the capitalization and add a period at the end', () => {
    //Arrange
    const input = "jOn SnoW, kINg IN thE noRth";
    const expected = "Jon Snow, King in the North.";

    //Act
    const actual = correctTitle(input);

    //Assert
    expect(actual).toBe(expected);
});


test('Should correct the capitalization and add a space after the comma', () => {
    //Arrange
    const input = "sansa stark,lady of winterfell.";
    const expected = "Sansa Stark, Lady of Winterfell.";

    //Act
    const actual = correctTitle(input);

    //Assert
    expect(actual).toBe(expected);
});

test('Should correct the capitalization', () => {
    //Arrange
    const input = "TYRION LANNISTER, HAND OF THE QUEEN.";
    const expected = "Tyrion Lannister, Hand of the Queen.";

    //Act
    const actual = correctTitle(input);

    //Assert
    expect(actual).toBe(expected);
});





