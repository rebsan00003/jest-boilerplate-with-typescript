import {isValidIP} from '.'

test('Should check if the string "1.2.3.4" matches IPv4 regex and return true', () => {
    //Arrange
    const input = "1.2.3.4";
    const expected = true;

    //Act
    const actual = isValidIP(input);

    //Assert
    expect(actual).toBe(expected);
});

test('Should check if the string "1.2.3" matches IPv4 regex and return false', () => {
    //Arrange
    const input = "1.2.3";
    const expected = false;

    //Act
    const actual = isValidIP(input);

    //Assert
    expect(actual).toBe(expected);
});

test('Should check if the string "1.2.3.4.5" matches IPv4 regex and return false', () => {
    //Arrange
    const input = "1.2.3.4.5";
    const expected = false;

    //Act
    const actual = isValidIP(input);

    //Assert
    expect(actual).toBe(expected);
});

test('Should check if the string "123.45.67.89" matches IPv4 regex and return false', () => {
    //Arrange
    const input = "123.45.67.89";
    const expected = true;

    //Act
    const actual = isValidIP(input);

    //Assert
    expect(actual).toBe(expected);
});

test('Should check if the string "123.456.78.90" matches IPv4 regex and return false', () => {
    //Arrange
    const input = "123.456.78.90";
    const expected = false;

    //Act
    const actual = isValidIP(input);

    //Assert
    expect(actual).toBe(expected);
});

test('Should check if the string "123.045.067.089" matches IPv4 regex and return false', () => {
    //Arrange
    const input = "123.045.067.089";
    const expected = false;

    //Act
    const actual = isValidIP(input);

    //Assert
    expect(actual).toBe(expected);
});