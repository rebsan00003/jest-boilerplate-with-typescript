export const anagrams = (word, wordArr) => {
    let anagramArr = new Array;
    let sortedWord = word.split('').sort().join('');

    if(word.length === 0 || wordArr.length === 0){
        console.log("Missing string or array");
    }else{
        for(let i = 0; i < wordArr.length; i++){
            if(wordArr[i].length === word.length){
                let sortedString = wordArr[i].split('').sort().join('');
                if(sortedWord === sortedString){
                    anagramArr.push(wordArr[i]);
                }
            }
        }
    }
    return anagramArr;
}
