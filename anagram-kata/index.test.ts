import { anagrams } from '.'

test('Should compare string "abba" with strings in array ["aabb", "abcd", "bbaa" "dada"] and return ["aabb", "bbaa"]', () => {
    //Arrange
    const inputFirst = 'abba';
    const inputSecond = ['aabb', 'abcd', 'bbaa', 'dada'];
    const expected = ['aabb', 'bbaa'];

    //Act
    const actual = anagrams(inputFirst, inputSecond);

    //Assert
    expect(actual).toEqual(expected);
});


test('Should compare string "racer" with strings in array ["crazer", "carer", "racar", "caers", "racer"] and return ["carer", "racer"]', () => {
    //Arrange
    const inputFirst = 'racer';
    const inputSecond = ['crazer', 'carer', 'racar', 'caers', 'racer'];
    const expected = ['carer', 'racer'];

    //Act
    const actual = anagrams(inputFirst, inputSecond);

    //Assert
    expect(actual).toEqual(expected);
});

test('Should compare string "laser" with strings in array ["lazing", "lazy", "lacer"] and return []', () => {
    //Arrange
    const inputFirst = 'laser';
    const inputSecond = ['lazing', 'lazy', 'lacer'];
    const expected = [];

    //Act
    const actual = anagrams(inputFirst, inputSecond);

    //Assert
    expect(actual).toEqual(expected);
});