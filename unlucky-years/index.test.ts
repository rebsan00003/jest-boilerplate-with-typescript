import {howUnlucky} from '.'

test('Check how many times friday the 13th appears in year 2020 and return 2', () => {
    //Arrange
    const input = 2020;
    const expected = 2;

    //Act
    const actual = howUnlucky(input);

    //Assert
    expect(actual).toBe(expected);
});

test('Check how many times friday the 13th appears in year 2026 and return 3', () => {
    //Arrange
    const input = 2026;
    const expected = 3;

    //Act
    const actual = howUnlucky(input);

    //Assert
    expect(actual).toBe(expected);
});

test('Check how many times friday the 13th appears in year 2016 and return 1', () => {
    //Arrange
    const input = 2016;
    const expected = 1;

    //Act
    const actual = howUnlucky(input);

    //Assert
    expect(actual).toBe(expected);
});