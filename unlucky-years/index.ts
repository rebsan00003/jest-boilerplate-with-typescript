export const howUnlucky = (date) => {
    let start = new Date(date, 0, 1);
    let end = new Date(date, 11, 31);
    let count = 0;
    let dateLoop = new Date(start);
    while (dateLoop <= end){ 
        if(dateLoop.getDay() === 5 && dateLoop.getDate() === 13){
            count++;
        }
        let newDate = dateLoop.setDate(dateLoop.getDate() + 1)
        dateLoop = new Date(newDate);
    }
    return count;
}