
const getNumArr = (num) => {
    let arr = String(num).split("").map((num) =>{
        return Number(num)
    })
    return arr;
}

export const happyAlgorithm = (num) => {
    let numArr = getNumArr(num);
    let numOfSteps = 0;
    let addNumArr = new Array;

    while(true){
        let addNum = 0;
        for(let i = 0; i < numArr.length; i++){
            addNum += Math.pow(numArr[i], 2);
        }
        numOfSteps++;
        if(addNum === 1){
            return "HAPPY " + numOfSteps;
        }else if(addNum === num || addNumArr.includes(addNum)){
            return "SAD " + numOfSteps;
        }
        numArr = getNumArr(addNum);
        addNumArr.push(addNum);
    }
}

