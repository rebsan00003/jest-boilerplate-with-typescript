import { happyAlgorithm } from '.'

test('Should transform number 139 until the number is equal to 1 and return HAPPY 5', () => {
    //Arrange
    const input = 139;
    const expected = "HAPPY 5";

    //Act
    const actual = happyAlgorithm(input);

    //Assert
    expect(actual).toBe(expected);
});

test('Should transform number 67 enters infinite loop and return SAD 10', () => {
    //Arrange
    const input = 67;
    const expected = "SAD 10";

    //Act
    const actual = happyAlgorithm(input);

    //Assert
    expect(actual).toBe(expected);
});

test('Should transform number 1 until the number is equal to 1 and return HAPPY 1', () => {
    //Arrange
    const input = 1;
    const expected = "HAPPY 1";

    //Act
    const actual = happyAlgorithm(input);

    //Assert
    expect(actual).toBe(expected);
});

test('Should transform number 89 until the number enters an infinite loop and return SAD 8', () => {
    //Arrange
    const input = 89;
    const expected = "SAD 8";

    //Act
    const actual = happyAlgorithm(input);

    //Assert
    expect(actual).toBe(expected);
});
