const faroShuffle = (deckArr) => {
    let shuffledDeck = deckArr.slice(0, deckArr.length/2).flatMap((element, i) => [element, deckArr[i + deckArr.length / 2]])

    return shuffledDeck;
}

export const shuffleCount = (num) => {
    let originalDeck = new Array;

    if(num < 2){
        throw new Error('Minimum size of deck is 2')
    }

    for(let i = 1; i <= num; i++){
        originalDeck.push(i)
    }

    let deck = faroShuffle(originalDeck)
    let numOfShuffles = 1;
    
    while(deck.toString() !== originalDeck.toString()){
        deck = faroShuffle(deck);
        numOfShuffles++;
    }

    return numOfShuffles;
}
