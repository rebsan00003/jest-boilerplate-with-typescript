import { shuffleCount } from ".";

test('Should shuffle a deck of cards containing 8 cards and return 3 as number of times the deck was shuffled', () => {
    //Arrange
    const input= 8;
    const expected = 3;

    //Act
    const actual = shuffleCount(input)

    //Assert
    expect(actual).toEqual(expected);
});

test('Should shuffle a deck of cards containing 14 cards and return 12 as number of times the deck was shuffled', () => {
    //Arrange
    const input= 14;
    const expected = 12;

    //Act
    const actual = shuffleCount(input)

    //Assert
    expect(actual).toEqual(expected);
});

test('Should shuffle a deck of cards containing 52 cards and return 8 as number of times the deck was shuffled', () => {
    //Arrange
    const input= 52;
    const expected = 8;

    //Act
    const actual = shuffleCount(input)

    //Assert
    expect(actual).toEqual(expected);
});